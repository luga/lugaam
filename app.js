const express = require('express')
const nunjucks = require('nunjucks')
const path = require('path')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const session = require('express-session')
const flash = require('connect-flash');
const mongoose = require('mongoose')
const moment = require('moment')
const passport = require('passport')
const LocalStrategy = require('passport-local/lib/strategy')

const Task = require('./models/tasks')
const User = require('./models/users')
const index = require('./routes/index')
const tasks = require('./routes/tasks')
const auth = require('./routes/auth')
const users = require('./routes/users')
const events = require('./routes/events')
const api = require('./routes/api')

const env = process.env.NODE_ENV || 'dev'
const app = express()

// Templating

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'html')
const template = nunjucks.configure('views', {
  autoescape: true,
  express: app
})
template.addFilter('date', (value, format) => {
  if (!value) return ''
  if (format === 'system') return `${value.getFullYear()}-${(value.getMonth() + 1).toString().padStart(2, 0)}-${value.getDate().toString().padStart(2, 0)}`
  return moment(value).format('DD/MM/YYYY')
})


// Middlewares

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(session({ secret: 'some secret' }))
app.use(flash())
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))


// Auth
app.use(passport.initialize())
app.use(passport.session())
passport.serializeUser((user, done) => done(null, user))
passport.deserializeUser(async (id, done) => {
  const instance = await User.findOne({ _id: id })
  return done(null, instance)
})
passport.use(new LocalStrategy((username, password, done) => {
  User.findOne({ email: username }, async (err, user) => {
    if (err) return done(err)
    if (!user) return done(null, false)
    user._previousLoginDate = user.latestLoginDate
    await user.updateLoginDate()
    return done(null, user)
  })
}))

// Global template variables
app.use(async (req, res, next) => {
  res.locals.currentUser = req.user
  const fromDate = req.user && req.user._previousLoginDate || new Date(2018, 6, 1) // 2018/07/01!
  const tasks = await Task.find({
    'comments.body': {$ne: ''},
    'comments.creationDate': { $gte: fromDate }
  }).populate('comments.author')
  res.locals.latestComments = [].concat(...tasks
    .map(t => {
      t.comments.forEach(c => c.task = t)
      return t.comments
    }))
    .filter(c => c.creationDate >= fromDate)
    .sort((a, b) => (a.creationDate > b.creationDate) ? -1 : 1)
  next()
})

// Routing

function isRouteAuthenticated(req, res, next) {
  req.isAuthenticated() ? next() : res.redirect('/auth')
}

app.use('/events', isRouteAuthenticated, events)
app.use('/task', isRouteAuthenticated, tasks)
app.use('/user', isRouteAuthenticated, users)
app.use('/api', isRouteAuthenticated, api)
app.use('/auth', auth)
app.use('/', isRouteAuthenticated, index)


// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = env === 'dev' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

mongoose.connect(process.env.MONGO_URI || 'mongodb://localhost/lugaam')

module.exports = app
