const express = require('express')
const router = express.Router()
const User = require('../models/users')


router.get('/', async (req, res, next) => {
  const users = await User.find({})
  res.render('user_list.html', { users })
})

router.get('/:id', async (req, res, next) => {
  const user = await User.findOne({_id: req.params.id})
  res.render('user.html', { user })
})

router.post('/:id/delete', async (req, res, next) => {
  await User.deleteOne({_id: req.params.id})
  res.redirect('/user')
})

router.post('/', save)
router.post('/:id', save)
async function save (req, res, next) {
  const id = req.params.id
  const data = req.body
  data.email = data.email.trim().toLowerCase()
  id ? await User.findOne({_id: id}).update(data)
     : await User(data).save()
  res.redirect('/user')
}

module.exports = router
