const express = require('express')
const router = express.Router()
const Task = require('../models/tasks')
const Event = require('../models/events')

router.get('/list.json', async (req, res, next) => {
  const tasks = await Task.find({})
  const events = tasks.map(task => new Event(task))
  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(events))
})

module.exports = router
