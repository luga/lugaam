const express = require('express')
const router = express.Router()
const Task = require('../models/tasks')


router.post('/task/:id', async (req, res, next) => {
  const data = req.body
  const task = await Task.findOne({_id: req.params.id})
  await task.update(data)
  res.json(await Task.findOne({_id: task.id}))
})

module.exports = router
