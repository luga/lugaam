const express = require('express')
const router = express.Router()
const passport = require('passport')


router.get('/', (req, res) => {
  const errors = req.flash('error') || []
  console.error(errors)
  res.render('auth.html', { errors })
})

router.post('/', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/auth',
  failureFlash: true,
  failureFlash: "L'adresse email n'est pas autorisée à accéder à l'application."
}))

router.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})


module.exports = router
