const express = require('express')
const router = express.Router()
const Task = require('../models/tasks')
const User = require('../models/users')


router.get('/:id', async (req, res, next) => {
  const task = await Task.findOne({_id: req.params.id})
    .populate('assignees')
    .populate('comments.author')
  const users = await User.find({})
  res.render('task.html', { task, users })
})

router.post('/', async (req, res, next) => {
  const data = req.body
  data.tags = data.tags.split(',').map(tag => {
    const cleaned = tag.trim()
    if (cleaned) return cleaned
  })
  await Task(data).save()
  res.redirect('/')
})

router.get('/:id/edit', async (req, res, next) => {
  const task = await Task.findOne({_id: req.params.id})
  res.render('task-edit.html', { task })
})

router.post('/:id', async (req, res, next) => {
  const data = req.body
  const task = await Task.findOne({_id: req.params.id})
  await task.update(data)
  res.redirect(`/task/${ task._id}`)
})

router.get('/:id/delete', async (req, res, next) => {
  const task = await Task.findOne({_id: req.params.id})
  await task.remove()
  res.redirect('/')
})

router.post('/:id/comment', async (req, res, next) => {
  const task = await Task.findOne({_id: req.params.id})
  const data = {
    author: req.user,
    body: req.body.body
  }
  if(data.body) {
    task.comments.push(data)
    await task.save()
  }
  res.redirect(`/task/${task._id}`)
})

router.post('/:id/assign', async (req, res) => {
  const task = await Task.findOne({_id: req.params.id})
  console.debug('***Body', req.body)
  if(req.body.action === 'create') task.addAssignee(req.user)
  else if(req.body.action === 'delete') task.removeAssignee(req.user)
  await task.save()
  res.redirect(`/task/${task._id}`)
})

module.exports = router
