const utils = require('../utils')
const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId
const User = require('./users')


class Comment {}

const commentSchema = new mongoose.Schema({
  creationDate: {
    type: Date,
    default: utils.utcnow
  },
  author: {type: ObjectId, ref: 'User'},
  body: String
})
commentSchema.loadClass(Comment)


const taskSchema = new mongoose.Schema({
  title: String,
  body: String,
  status: {
    type: String,
    default: 'todo'
  },
  assignees: [{type: ObjectId, ref: 'User'}],
  tags: {
    type: Array,
    default: []
  },
  startDate: Date,
  endDate: Date,
  creationDate: {
    type: Date,
    default: utils.utcnow
  },
  comments: [commentSchema]
})

class Task {
  getTags () {
    return this.tags.map(tag => {
      if (!tag) return
      const cleaned = tag.trim().split(':').map(part => part.trim())
      if (cleaned.length > 1) {
        return { type: cleaned[0], label: cleaned[1] }
      }
      return { label: cleaned[0] }
    })
  }

  addAssignee(user) {
    if(!this.isAssignedTo(user)) this.assignees.push(user)
  }

  removeAssignee(user) {
    const assigneeIds = this.assignees.map(a => String(a._id))
    this.assignees.splice(assigneeIds.indexOf((user.id || user)), 1)
  }

  isAssignedTo(user) {
    return this.assignees.some(a => String(a._id) === (user.id || user))
  }

  getTagLabels () {
    return this.getTags().map(t => t && t.label)
  }

  isImportant () {
    return this.getTagLabels().includes('important')
  }
}

taskSchema.loadClass(Task)
module.exports = mongoose.model('Task', taskSchema)
