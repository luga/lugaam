class Event {
  /**
  * Convert a Task into a FullCalendatJS event.
  */
  constructor (task) {
    this.task = task
    this.id = task.id
    this.title = task.title
    this.start = task.startDate || this.creationDate
    this.end = task.endDate
    this.url = `/task/${this.id}`
    this.allDay = true
    this.color = this.getColor()
  }

  getColor () {
    if(this.task.status == 'progress') return '#229650'
    if(this.task.status == 'done') return '#a1a1a1'
    if(this.task.isImportant()) return 'rgba(200, 30, 60, .8)'
  }

  get (field) {
    return this.task[field]
  }
}

module.exports = Event
