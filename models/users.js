const utils = require('../utils')
const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
  infos: String,
  roles: Array,
  creationDate: {
    type: Date,
    default: utils.utcnow
  },
  latestLoginDate: Date,
  _previousLoginDate: Date, // not persistent
})

class User {
  display() {
    return this.name || this.email || `Sans nom (${this.id})`
  }

  toString() {
    return this.display()
  }

  equals(compare) {
    return compare && this.id.toString() == compare.id.toString()
  }

  isAdmin() {
    return this.roles.includes('admin')
  }

  async updateLoginDate() {
    this.latestLoginDate = utils.utcnow()
    await this.save()
  }
}

schema.loadClass(User)
module.exports = mongoose.model('User', schema)
