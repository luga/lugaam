document.querySelectorAll('[type=date]').forEach(field => {
  new Pikaday({ field,
    firstDay: 1,
    i18n: {
      previousMonth: 'Mois précédent',
      nextMonth: 'Mois suivant',
      months: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
      weekdays: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
      weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam']
    }
  })
})
